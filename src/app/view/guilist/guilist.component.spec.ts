import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuilistComponent } from './guilist.component';

describe('GuilistComponent', () => {
  let component: GuilistComponent;
  let fixture: ComponentFixture<GuilistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuilistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuilistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
